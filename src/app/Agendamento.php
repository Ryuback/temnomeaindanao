<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agendamento extends Model
{
    protected $table = 'agendamento';
    protected $fillable = ['paciente_id','clinica_id','user_id','data','horario','titulo'];
}
