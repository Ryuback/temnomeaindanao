<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    // Nome da tabela  e seus respectivos atributos.
    protected $table = 'clinica';
    protected $fillable = ['nome','cnpj','endereco','telefone'];


    // Função que gerencia as tabelas de ligação
    public function buscarPaciente()
    {
        return $this->belongsToMany('App\Paciente','clinica_has_paciente','clinica_id','paciente_id');
    }

    public function buscarUser()
    {
        return $this->belongsToMany('App\User','usuario_has_clinica','clinica_id','usuario_id');
    }

}

