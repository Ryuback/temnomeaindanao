<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dente extends Model
{
    protected $table = 'dente';
    protected $fillable = ['nome'];
}
