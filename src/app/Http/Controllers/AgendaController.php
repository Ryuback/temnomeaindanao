<?php

namespace App\Http\Controllers;

use App\Agendamento;
use App\Paciente;
use App\Clinica;
use App\User;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user_id = auth()->user()->id;
        
        $agendamento = Agendamento::where('user_id',$user_id)->get();
        $user = User::find($user_id);
        $clinicas = $user->buscarClinica;
        foreach ($clinicas as $clinica) {
            $paciente = $clinica->buscarPaciente;
            return view('agenda', compact('agendamento','paciente','clinicas','user_id'))->with('i', (request()->input('page', 1) - 1) * 50);
        }


        // $agendamento = Agendamento::paginate(50);
        // $paciente = Paciente::paginate(50);
        // $clinica = Clinica::paginate(50);


        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agendamento = $request->all();
        $agendamento = Agendamento::create($agendamento);
        return redirect()->route('agenda.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agendamento::destroy($id);
        return redirect()->route('agenda.index');
    }

    public function confirmexclude($id)
    {
        $id = $id;
        return view('confirmexclude', compact('id'));
    }
}
