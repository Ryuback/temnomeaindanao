<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Anamnese;
use App\Paciente;
use App\Orcamento;
use App\Prontuario;
use App\Clinica;


class AnamneseController extends Controller
{
    public function requestdata(Request $request)
    {
        

        $orcamento_id = $request->session()->get('orcamento');
        $orcamento = Orcamento::find($orcamento_id->id);

        $nome = $orcamento->nome;
        $cpf = $orcamento->cpf;
        $data = $request->data;
        $telefone = $request->telefone;
        $cep = $request->cep;
        $endereco =  $request->endereco;
        $email = $request->email;
        $sexo = $request->sexo;

        $paciente = Paciente::create([
            'nome' => $nome,
            'cpf' => $cpf,
            'datanascimento' => $data,
            'telefone' => $telefone,
            'cep' => $cep,
            'endereco' => $endereco,
            'email' => $email,
            'sexo' => $sexo,
        ]);

        $prontuatrio = Prontuario::create([
            'obs' => $request->obs
        ]);

        $paciente_id = Paciente::find($paciente->id);
        $paciente_id->buscarProntuario()->attach($prontuatrio->id);

        $clinica_id = $request->session()->get('clinica_id');
        $clinica = Clinica::find($clinica_id);
        $clinica->buscarPaciente()->attach($paciente->id);

        $prontuatrio->buscarOrcamento()->attach($orcamento->id);

        $valor1 = $request->pergunta1;
        $valor2 = $request->pergunta2;
        $valor3 = $request->pergunta3;
        $valor4 = $request->pergunta4;
        $valor5 = $request->pergunta5;
        $valor6 = $request->pergunta6;
        $valor7 = $request->pergunta7;
        $valor8 = $request->pergunta8;
        $valor9 = $request->pergunta9;
        $valor10 = $request->pergunta10;
        $valor11 = $request->pergunta11;
        $valor12 = $request->pergunta12;
        $valor13 =$request->pergunta13;
        $valor14 = $request->pergunta14;
        $valor15 = $request->pergunta15;
        $valor16 = $request->pergunta16;

        $anamnese = Anamnese::insert([
            [
                'pergunta' => 1 ,
                'resposta' => $valor1,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 2 ,
                'resposta' => $valor2,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 3 ,
                'resposta' => $valor3,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 4 ,
                'resposta' => $valor4,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 5 ,
                'resposta' => $valor5,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 6 ,
                'resposta' => $valor6,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 7 ,
                'resposta' => $valor7,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 8 ,
                'resposta' => $valor8,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 9 ,
                'resposta' => $valor9,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 10 ,
                'resposta' => $valor10,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 11 ,
                'resposta' => $valor11,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 12 ,
                'resposta' => $valor12,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 13 ,
                'resposta' => $valor13,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 14 ,
                'resposta' => $valor14,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 15 ,
                'resposta' => $valor15,
                'prontuario_id' => $prontuatrio->id
            ],
            [
                'pergunta' => 16 ,
                'resposta' => $valor16,
                'prontuario_id' => $prontuatrio->id
            ]
        ]);

        return redirect()->route('pacientes.index');

    }
}
