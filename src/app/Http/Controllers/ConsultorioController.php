<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clinica;
use App\User;
use App\Orcamento;

class ConsultorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = auth()->user()->id;
        $user_id = User::find($user_id); 
        $request->session()->get('clinica_id');

        $clinica_id = $request->session()->get('clinica_id');
        $clinica_id = Clinica::find($clinica_id);


        
        // print_r($user);
         return view('selecionarconsultorio', compact('clinica_id','user_id'))->with('i', (request()->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth()->user();
       // $user_id = User::find($user_id);
        $clinica = $request->all();
        $criarclinica = Clinica::create($clinica);
        $clinica_id = Clinica::find($criarclinica->id);
        echo $clinica_id->id;
        $clinica_id->buscarUser()->attach($user_id->id);

       return redirect()->route('selecionarconsultorio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $clinica = Clinica::find($id);

        $clinica->buscarUser()->detach();

        $request->session()->put('clinica_id', null);

        return redirect()->route('selecionarconsultorio.index');


    }
    public function teste(Request $request)
    {
        $request->session()->put('clinica_id',$request->id);
        $clinica_id = $request->session()->get('clinica_id');

        return redirect()->route('selecionarconsultorio.index');

    }


}
