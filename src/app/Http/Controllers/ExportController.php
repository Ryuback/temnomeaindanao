<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Clinica;
use App\User;
use App\Orcamento;
use App\Paciente;
use App\Prontuario;
use Illuminate\Support\Facades\DB;

class ExportController extends Controller
{
    public function index(Request $request){

        $clinica = $request->session()->get('clinica_id');
        $clinica = Clinica::find($clinica);

        $user_id = auth()->user()->id;
        $users = User::find($user_id);

        $id = $request->input('id');

        $cliente = Orcamento::find($id);
        $dente_orcamento = DB::table('orcamento_has_dente')->where('orcamento_id', $cliente->id)->get();

        
        $orcamentos = $request->session()->get('orcamento');
        $orcamento = $orcamentos->id;

        return view('imprimeorcamento', compact('clinica','users','cliente','dente_orcamento','orcamento'))->with('i', (request()->input('page', 1) - 1) * 50);


    }

    public function exportprontuario(Request $request){

        $clinica = $request->session()->get('clinica_id');
        $clinica = Clinica::find($clinica);

        $user_id = auth()->user()->id;
        $users = User::find($user_id);

        $id = $request->input('id');

        $paciente = Paciente::find($id);

        $busca_prontuario = $paciente->buscarProntuario;

        foreach ($busca_prontuario as $busca_prontuarios) {
            $busca_prontuario = Prontuario::find($busca_prontuarios->id);
            $busca_orcamento = $busca_prontuario->buscarOrcamento;
            return view('imprimeprontuario', compact('clinica','users','paciente','busca_orcamento','busca_prontuario'))->with('i', (request()->input('page', 1) - 1) * 50);
             }
        



    }
}
