<?php

namespace App\Http\Controllers;

use App\Clinica;
use Illuminate\Http\Request;
// LEMBRAR DE BOTAR A CLASSE QUE ESTÁ BUSCANDO
use App\Dente;
use App\Orcamento;
use App\Especialidade;
use App\Procedimento;
use App\Paciente;
use App\Prontuario;
use Illuminate\Support\Facades\DB;

class OrcamentoController extends Controller
{
    // Tudo que contem dentro dessa classe é que forma o controller pode-se dizer.

    public function index(Request $request) // index ele é página principal desse controller.
    {// $nomedavariável = nome do modelo.php (é onde vou chamar a classe criar dentro de Http)

        $dente1 = Dente::paginate(16);
        $dente2 = Dente::where('id','>',16)->get();
        $especialidade = Especialidade::paginate(10);

        $clinica_id =  $request->session()->get('clinica_id');

        $clinica = Clinica::find($clinica_id);

        $paciente = $clinica->buscarPaciente;
        
       //$paciente = Paciente::paginate(50);

        //paginate(50) = serve pra carregar uma certa quantidade de  linha de dados.

        $procedimento = Procedimento::paginate(50);

        $busca_procedimento = $request->session()->get('orcamento');
        $dente_orcamento = DB::table('orcamento_has_dente')->where('orcamento_id', $busca_procedimento->id)->get();

        $orcamentos = $request->session()->get('orcamento');
        $orcamento = $orcamentos->id;
        $busca_procedimento = Orcamento::find($busca_procedimento->id);
        //$busca_procedimento = $busca_procedimento->buscarProcedimento;


        
        //Dentro do compact ele puxa a variável criada acimam exemplo: $dente
        // Return view "Nome da página ou pasta criar dentro de resources->views"
        return view('novoorcamento', compact('dente1','dente2','especialidade','procedimento','paciente','busca_procedimento','dente_orcamento','orcamento'))->with('i', (request()->input('page', 1) - 1) * 50);
        
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {

        
        // return $request->all();
        // $orcamento = Orcamento::create($request->all());
        // $orcamento_id = Orcamento::find($orcamento->id);
        // $teste = $orcamento_id->id;

        $orcamento_id = $request->session()->get('orcamento');
        $valor =  Orcamento::find($orcamento_id->id);
        $valor = $valor->preco;
        Orcamento::where('id', $orcamento_id->id)->update(['preco' =>$request->valor + $valor  ]);
         $dente_id = $request->dente_id;
         $procedimento_id = $request->procedimento_id;
         $valor = $request->valor;
        foreach($dente_id as $dente){
          $orcamento_id->buscarOrcamento()->attach($orcamento_id, array('orcamento_id' => $orcamento_id->id,'dente_id' => $dente, 'procedimento_id' => $procedimento_id));
         }

          return redirect()->route('novoorcamento.index');

         

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function informardados(Request $request){
        $clinica_id =  $request->session()->get('clinica_id');
        if ($clinica_id == null) {
            return redirect()->route('selecionarconsultorio.index')->with('success', 'É necessário selecionar um consultório para acessar essa área!');
        }

        else{
            return view('dadosorcamento');
        }
    }
    public function solicitadados(Request $request )
    {

        $name = $request->input('nome');
        $cpf = $request->input('cpf');
        $teste =  $request->session()->get('clinica_id');

        $orcamento = Orcamento::create([
            'clinica_id' => $request->session()->get('clinica_id'),
            'nome'=>$name,
            'cpf'=>$cpf
            ]);

        $orcamento = Orcamento::find($orcamento->id);

        $request->session()->put('orcamento',$orcamento);


        return redirect()->route('novoorcamento.index');

       
    }
    public function buscaorcamento(Request $request )
    {
        $clinica_id = $request->session()->get('clinica_id');

        if ($clinica_id == null) {
            return redirect()->route('selecionarconsultorio.index')->with('success', 'É necessário selecionar um consultório para acessar essa área!');
        }else{

        $orcamento = Orcamento::where('clinica_id',$clinica_id)->get();

        //return $orcamento;

        return view('buscarorcamento', compact('orcamento'))->with('i', (request()->input('page', 1) - 1) * 50);
        }
       
    }
    public function novoprontuario(Request $request )
    {

        $orcamento_id = $request->session()->get('orcamento');
        $orcamento = Orcamento::find($orcamento_id->id);

        return view('novoprontuario', compact('orcamento'))->with('i', (request()->input('page', 1) - 1) * 50);
     }

     public function inclueorcamento(Request $request )
     {

        $paciente_id = $request->input('paciente');

        $paciente = Paciente::find($paciente_id);
        $busca_prontuario = $paciente->buscarProntuario;

        foreach ($busca_prontuario as $busca_prontuarios) {
            $busca_prontuario = Prontuario::find($busca_prontuarios->id);
            
                       $orcamento_id = $request->session()->get('orcamento');
                       $orcamento = Orcamento::find($orcamento_id->id);

                $busca_prontuario->buscarOrcamento()->attach($orcamento);

                return redirect()->route('pacientes.index');
            
        }
 

      }


       
    }
    


