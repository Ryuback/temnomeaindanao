<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Clinica;

class PacientesController extends Controller
{
    public function index(Request $request)
    {

    $clinica_id =  $request->session()->get('clinica_id');

    if ($clinica_id == null) {
        return redirect()->route('selecionarconsultorio.index')->with('success', 'É necessário selecionar um consultório para acessar essa área!');
    }else{


    $clinica = Clinica::find($clinica_id);

    $paciente = $clinica->buscarPaciente()->paginate(50);
    

    return view('pacientes', compact('paciente'))->with('i', (request()->input('page', 1) - 1) * 50);

    }

    }


}
