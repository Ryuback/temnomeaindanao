<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

class PerfilController extends Controller
{
    public function index(Request $request){

        $user_id = auth()->user()->id;
        $user = User::find($user_id);

        return view('perfil', compact('user'));


    }

    public function updateperfil(Request $request){

        $senha = $request->senha;
        $request->confirmarsenha;

        $user_id = auth()->user()->id;
        User::where('id',$user_id)->update(['password' => $senha]);

        return redirect()->route('perfil.index');;


    }


}
