<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $table = 'orcamento';
    protected $fillable = ['nome','cpf','preco','clinica_id'];

    public function buscarOrcamento()
    {
        return $this->belongsToMany('App\Orcamento','orcamento_has_dente','orcamento_id','dente_id','procedimento_id','valor');
    }

    public function buscarProcedimento()
    {
        return $this->belongsToMany('App\Procedimento','orcamento_has_dente','orcamento_id','procedimento_id');
    }

    public function buscarDente()
    {
        return $this->belongsToMany('App\Procedimento','orcamento_has_dente','dente_id','procedimento_id');
    }
    
}
