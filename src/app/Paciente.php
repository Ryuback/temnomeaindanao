<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paciente extends Model
{
    protected $table = 'paciente';
    protected $fillable = ['nome','cpf','datanascimento','telefone','sexo','cep','endereco','email'];

    public function buscarProntuario()
    {
        return $this->belongsToMany('App\Prontuario','prontuario_has_paciente','prontuario_id','paciente_id');
    }
}

