<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedimento extends Model
{
    protected $table = 'procedimento';
    protected $fillable = ['nome','especialidade_id','nome_id'];
}
