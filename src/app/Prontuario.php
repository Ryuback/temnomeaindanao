<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prontuario extends Model
{
    protected $table = 'prontuario';
    protected $fillable = ['obs'];

    public function buscarOrcamento()
    {
        return $this->belongsToMany('App\Orcamento','prontuario_has_orcamento','prontuario_id','orcamento_id');
    }
}
