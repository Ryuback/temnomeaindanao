<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $fillable = ['nome','email','senha','telefone','cro'];

    public function buscarClinica()
    {
        return $this->belongsToMany('App\Clinica','usuario_has_clinica','usuario_id','clinica_id');
    }
}
