<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bancotcc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Criação das tabelas (principais do banco)

        // Schema::create('usuario', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('nome');
        //     $table->string('email');
        //     $table->string('senha');
        //     $table->string('telefone');
        //     $table->string('cro');
        //     $table->timestamps(); //sempre que alterar qualquer coisa, ele sempre salva a hora que foi alterado e a data também. (Sempre ter em todas as tabelas)
        // });

        // Tabela clinica
        Schema::create('clinica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cnpj');
            $table->string('endereco');
            $table->string('telefone');
            $table->timestamps();
        });

         // Tabela paciente
         Schema::create('paciente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cpf');
            $table->date('datanascimento');
            $table->string('telefone');
            $table->string('sexo');
            $table->string('cep');
            $table->string('endereco');
            $table->string('email');
            $table->timestamps();
        });

        //Tabela agendamento
        Schema::create('agendamento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('data');
            $table->string('horario');
            $table->string('titulo');
            $table->unsignedBigInteger('paciente_id')->nullable(); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('paciente_id')->references('id')->on('paciente');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->unsignedBigInteger('clinica_id')->nullable(); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('clinica_id')->references('id')->on('clinica');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->unsignedBigInteger('user_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('user_id')->references('id')->on('users');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->timestamps();
        });


        //Tabela prontuario
        Schema::create('prontuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('obs')->nullable();
            $table->timestamps();
        });
         
        // Tabela orcamento
        Schema::create('orcamento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('clinica_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('clinica_id')->references('id')->on('clinica');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->string('cpf');
            $table->string('nome');
            $table->double('preco')->nullable();
            $table->timestamps();
        });
        // Tabela dente
        Schema::create('dente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('valor');
            $table->string('valor2')->nullable();
            $table->timestamps();
        });

        // Tabela especialidade 
            Schema::create('especialidade', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->timestamps();
            });

        // Tabela procedimento 
        Schema::create('procedimento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('nome_id');
            $table->unsignedBigInteger('especialidade_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('especialidade_id')->references('id')->on('especialidade');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->timestamps();
        });

        Schema::create('anamnese', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('resposta');
            $table->string('pergunta');
            $table->unsignedBigInteger('prontuario_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('prontuario_id')->references('id')->on('prontuario');// vincula a chave estrangeria ao ID da tabela selecionada
            $table->timestamps();
        });

 

////////////////////////////////////////////////////////////////////////////////////////////////////////
        // criação das tabelas de ligação do banco 

        //Tabela de ligação Usuario_has_Clinica
                        //tabela de ligação
        Schema::create('usuario_has_clinica', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('usuario_id')->references('id')->on('users');// vincula a chave estrangeria ao ID da tabela selecionada

            $table->unsignedBigInteger('clinica_id');
            $table->foreign('clinica_id')->references('id')->on('clinica');
        });
        //Tabela de ligação clinica_has_paciente
        Schema::create('clinica_has_paciente', function (Blueprint $table) {
            $table->unsignedBigInteger('clinica_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('clinica_id')->references('id')->on('clinica');// vincula a chave estrangeria ao ID da tabela selecionada

            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id')->references('id')->on('paciente');
        });
        //Tabela de ligação prontuario_has_paciente
        Schema::create('prontuario_has_paciente', function (Blueprint $table) {
            $table->unsignedBigInteger('prontuario_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('prontuario_id')->references('id')->on('prontuario');// vincula a chave estrangeria ao ID da tabela selecionada

            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id')->references('id')->on('paciente');
        });
        //Tabela de ligação prontuario_has_orcamento
        Schema::create('prontuario_has_orcamento', function (Blueprint $table) {
            $table->unsignedBigInteger('prontuario_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('prontuario_id')->references('id')->on('prontuario');// vincula a chave estrangeria ao ID da tabela selecionada

            $table->unsignedBigInteger('orcamento_id');
            $table->foreign('orcamento_id')->references('id')->on('orcamento');
        });
        //Tabela de ligação orcamento_has_dente
        Schema::create('orcamento_has_dente', function (Blueprint $table) {
            $table->unsignedBigInteger('orcamento_id'); // Ele cria a chave estrangeira "automaticamente"
            $table->foreign('orcamento_id')->references('id')->on('orcamento');// vincula a chave estrangeria ao ID da tabela selecionada

            $table->unsignedBigInteger('dente_id');
            $table->foreign('dente_id')->references('id')->on('dente');

            $table->unsignedBigInteger('procedimento_id');
            $table->foreign('procedimento_id')->references('id')->on('procedimento');

        });

                // INSERT AFTER MIGRATION
                // Dentes
                DB::table('dente')->insert(array('nome' => 'dente18','valor' => '18', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente17','valor' => '17', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente16','valor' => '16', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente15','valor' => '15', 'valor2'=> '55'));
                DB::table('dente')->insert(array('nome' => 'dente14','valor' => '14', 'valor2'=> '54'));
                DB::table('dente')->insert(array('nome' => 'dente13','valor' => '13', 'valor2'=> '53'));
                DB::table('dente')->insert(array('nome' => 'dente12','valor' => '12', 'valor2'=> '52'));
                DB::table('dente')->insert(array('nome' => 'dente11','valor' => '11', 'valor2'=> '51'));
                DB::table('dente')->insert(array('nome' => 'dente21','valor' => '21', 'valor2'=> '61'));
                DB::table('dente')->insert(array('nome' => 'dente22','valor' => '22', 'valor2'=> '62'));
                DB::table('dente')->insert(array('nome' => 'dente23','valor' => '23', 'valor2'=> '63'));
                DB::table('dente')->insert(array('nome' => 'dente24','valor' => '24', 'valor2'=> '64'));
                DB::table('dente')->insert(array('nome' => 'dente25','valor' => '25', 'valor2'=> '65'));
                DB::table('dente')->insert(array('nome' => 'dente26','valor' => '26', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente27','valor' => '27', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente28','valor' => '28', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente48','valor' => '48', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente47','valor' => '47', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente46','valor' => '46', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente45','valor' => '45', 'valor2'=> '85'));
                DB::table('dente')->insert(array('nome' => 'dente44','valor' => '44', 'valor2'=> '84'));
                DB::table('dente')->insert(array('nome' => 'dente43','valor' => '43', 'valor2'=> '83'));
                DB::table('dente')->insert(array('nome' => 'dente42','valor' => '42', 'valor2'=> '82'));
                DB::table('dente')->insert(array('nome' => 'dente41','valor' => '41', 'valor2'=> '81'));
                DB::table('dente')->insert(array('nome' => 'dente31','valor' => '31', 'valor2'=> '71'));
                DB::table('dente')->insert(array('nome' => 'dente32','valor' => '32', 'valor2'=> '72'));
                DB::table('dente')->insert(array('nome' => 'dente33','valor' => '33', 'valor2'=> '73'));
                DB::table('dente')->insert(array('nome' => 'dente34','valor' => '34', 'valor2'=> '74'));
                DB::table('dente')->insert(array('nome' => 'dente35','valor' => '35', 'valor2'=> '75'));
                DB::table('dente')->insert(array('nome' => 'dente36','valor' => '36', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente37','valor' => '37', 'valor2'=> ''));
                DB::table('dente')->insert(array('nome' => 'dente38','valor' => '38', 'valor2'=> ''));
                //Especialidades
                DB::table('especialidade')->insert(array('nome' => 'Ortodontia'));
                DB::table('especialidade')->insert(array('nome' => 'Pediatria'));
                DB::table('especialidade')->insert(array('nome' => 'Prótese'));
                DB::table('especialidade')->insert(array('nome' => 'Implantodontia'));
                //Procedimentos
                DB::table('procedimento')->insert(array('nome' => 'Aparelho Extra Bucal','especialidade_id' =>'1','nome_id' => 'aparelhoextrabucal'));
                DB::table('procedimento')->insert(array('nome' => 'Arco Lingual','especialidade_id' =>'1','nome_id' => 'arcolingual'));
                DB::table('procedimento')->insert(array('nome' => 'Bimler','especialidade_id' =>'1','nome_id' => 'bimler'));
                DB::table('procedimento')->insert(array('nome' => 'Grade Palatina Fixa','especialidade_id' =>'1','nome_id' => 'gradepalatinafixa'));

                DB::table('procedimento')->insert(array('nome' => 'Aparelho Ortodôntico Fixo','especialidade_id' =>'2', 'nome_id' => 'aparelhoortodonticofixo'));
                DB::table('procedimento')->insert(array('nome' => 'Coroa de Aço','especialidade_id' =>'2','nome_id' => 'coroadeaco'));
                DB::table('procedimento')->insert(array('nome' => 'Mantenedor de Espaço','especialidade_id' =>'2','nome_id' => 'mantenedordeespaco'));
                DB::table('procedimento')->insert(array('nome' => 'Restauração em Resina','especialidade_id' =>'2','nome_id' => 'restauracaoemresina'));

                DB::table('procedimento')->insert(array('nome' => 'Coroa 3/4 ou 4/5 (Bloco)','especialidade_id' =>'3','nome_id' => 'coroa34ou45bloco'));
                DB::table('procedimento')->insert(array('nome' => 'Coroa de Veneer','especialidade_id' =>'3','nome_id' => 'coroadeveneer'));
                DB::table('procedimento')->insert(array('nome' => 'Coroa Total Metálica','especialidade_id' =>'3','nome_id' => 'coroatotalmetalica'));
                DB::table('procedimento')->insert(array('nome' => 'Facetas Laminadas de Porcelana','especialidade_id' =>'3','nome_id' => 'facetaslaminadasdeporcelana'));

                DB::table('procedimento')->insert(array('nome' => 'Cicatrizador','especialidade_id' =>'4','nome_id' => 'cicatrizador'));
                DB::table('procedimento')->insert(array('nome' => 'Implante cilíndrico tratado','especialidade_id' =>'4','nome_id' => 'implantecilindricotratado'));
                DB::table('procedimento')->insert(array('nome' => 'Implante Nobel Perfect','especialidade_id' =>'4','nome_id' => 'implantenobelperfect'));
                DB::table('procedimento')->insert(array('nome' => 'Implante rosqueável não tratado','especialidade_id' =>'4','nome_id' => 'implanterosqueavelnaotratado'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orcamento_has_dente');
        Schema::drop('prontuario_has_orcamento');
        Schema::drop('prontuario_has_paciente');
        Schema::drop('clinica_has_paciente');
        Schema::drop('usuario_has_clinica');
        Schema::drop('procedimento');
        Schema::drop('especialidade');
        Schema::drop('dente');
        Schema::drop('orcamento');
        Schema::drop('prontuario');
        Schema::drop('agendamento');
        Schema::drop('paciente');
        Schema::drop('clinica');
        Schema::drop('usuario');


    }
}
