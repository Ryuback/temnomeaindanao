@extends('template')

@section('title', 'Agendar consulta')
@section('content')
<div class="container-fluid">

          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-3">

	              <!-- Overflow Hidden -->
	              <div class="card mb-4">
	                <div class="card-header py-3">
	                  <h6 class="m-0 font-weight-bold text-primary corprontuario">Adicionar agendamento</h6>
	                </div>
	                <div class="card-body">
						{!! Form::open(['route' => 'agenda.store', 'method' => 'POST']) !!}

						<input type="hidden" name="user_id"value="{{$user_id}}">

						  <div class="form-group">
						    <label for="nome">Título</label>
						    <input type="text" id="nome" name="titulo" class="form-control" aria-describedby="emailHelp">
						    <small id="emailHelp" class="form-text text-muted">Informe o título do seu agendamento</small>
						  </div>
						  <div class="form-group">
						    <label for="Paciente">Paciente</label>
							<select  class="selectpicker" name="paciente_id">
							<option value=""></option> 
								@foreach ($paciente as $pacientes)
								<option value="{{$pacientes->id}}">{{$pacientes->nome}}</option> 
								@endforeach
							  </select>
						  </div>
						  <div class="form-group">
						    <label for="Clinica">Clinica</label>
							<select class="selectpicker" name="clinica_id">
							<option value=""></option> 
								@foreach ($clinicas as $clinica)
								<option value="{{$clinica->id}}">{{$clinica->nome}}</option> 
								@endforeach 
							  </select>
						  </div>
						  
						  <div class="form-group">
						    <label for="data">Data</label>
						    <input type="date" id="data" name="data" class="form-control">
						  </div>
						  
						  <div class="form-group">
						    <label for="hora">Hora</label>
						    <input type="time" id="hora" name="horario" class="form-control">
						  </div>
						  
						  <button type="submit" class="btn btn-primary">Salvar agendamento</button>
						  
						  {!! Form::close()!!}
	                </div>
	              </div>

	            </div>


                <div class="col-lg-9">

	              <!-- Overflow Hidden -->
	              <div class="card mb-4">
	                <div class="card-header py-3">
	                  <h6 class="m-0 font-weight-bold text-primary corprontuario">Calendário</h6>
	                </div>
	                <div class="card-body">
	                  <div id='calendar'></div>
	                </div>
	              </div>

	            </div>


				 <!-- @foreach ($agendamento as $agendamentos)
				<input type="hidden" name="" value="{{$busca_id_clinica =  $agendamentos->clinica_id}}">
				<input type="hidden" name="" value="{{$busca_clinica = App\Clinica::find($busca_id_clinica)}}">
				
				@endforeach -->
          </div>

        </div>
@endsection


<script>

		var agendamentos = [
		@foreach ($agendamento as $agendamentos)
		{
		    title: '{{$agendamentos->titulo}} \n @if($agendamentos->clinica_id == null)@else{{$busca_clinica = App\Clinica::find($agendamentos->clinica_id)->nome}}@endif ',
		    start: '{{$agendamentos->data}}T{{$agendamentos->horario}}',
		    backgroundColor: '#4b678e',
		    textColor: '#ffffff',
			url:'confirmexclude/{{$agendamentos -> id}}'
		},
		@endforeach
		];

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid' ],
          locale: 'pt-br',
          events: agendamentos
        });

        calendar.render();
      });

</script>
