@extends('template')

@section('title', 'Buscar orçamento')
@section('content')
<div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary corbusca">Buscar Orçamentos</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Data de Lançamento</th>
                      <th>Orçamento</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Data de Lançamento</th>
                      <th>Orçamento</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  @foreach($orcamento as $orcamentos)
                  <tr>
                      <td>{{$orcamentos->nome}}</td>
                      <td>{{$orcamentos->cpf}}</td>
                      <td>{{date('d/m/Y "H:i:s"', strtotime($orcamentos->created_at))}}</td>
                      <td>
                        <a href="#" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                            <i class="fas fa-file-pdf"></i>
                          </span>
                          <form method="POST" action="{{ URL::to('/export') }}">
                          @csrf
                          <input type="hidden" value="{{$orcamentos->id}}" name="id">
                          <button type="submit" class="text">Abrir</button>
                          </form>
                        </a>
                      </td>
                    </tr>
                  @endforeach

                
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
@endsection
