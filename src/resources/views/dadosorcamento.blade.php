@extends ('template')
@section('title', 'Selecionar Consultório')
@section('content')

<div class="container-fluid">

	<!-- Content Row -->
	<div class="row d-flex justify-content-center">

        <div class="col-md-10 col-xl-6">

			<div class="card mb-12">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary corprontuario">Informe os dados do paciente</h6>
				</div>
				<div class="card-body">

					<form  style="width: 100%;border:none;outline:none ;background-color:#fff;" method="POST" action="{{ URL::to('/solicitadados') }}">
						@csrf
						<div class="form-group">
							<label for="name">Nome*</label>
							<input type="text" class="form-control" name="nome" placeholder="Digite o nome" required>
						</div>
						<div class="form-group">
							<label for="cpf">CPF*</label>
							<input type="text" class="form-control" name="cpf" placeholder="Digite o CPF" data-mask="000.000.000-00" required>
						</div>

						<div style="float: right; bottom: 0; position: relative; margin-bottom: 15px">
							<button type="submit" class="btn btn-primary" value="Input" >Próximo</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection