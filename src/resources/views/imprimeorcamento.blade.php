<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Orçamento</title>

    <style type="text/css">
        body {
    background-color: #000
}

.padding {
    padding: 2rem !important
}

.card {
    margin-bottom: 30px;
    border: none;
    -webkit-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    -moz-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22)
}

.card-header {
    background-color: #fff;
    border-bottom: 1px solid #e6e6f2
}

h3 {
    font-size: 20px
}

h5 {
    font-size: 15px;
    line-height: 26px;
    color: #3d405c;
    margin: 0px 0px 15px 0px;
    font-family: 'Circular Std Medium'
}

.text-dark {
    color: #3d405c !important
}

.imagens {
    border-top: 1px solid #e6e6f2;
}

.assinatura{
    margin-bottom: 80px;
    margin-top: 50px;
}
    </style>
  </head>
  <body>



 <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
     <div class="card">
         <div class="card-header p-4">

           <h3 class="mb-0 float-left" >Dr.
           {{$users->name}}
           </h3> 

             <div class="float-right">
                 <h3 class="mb-0">CRODF {{$users->cro}}</h3>
                 
             </div>
         </div>
         <div class="card-body">
             <div class="row mb-4">
                 <div class="col-sm-6">
                    
                     <h3 class="text-dark mb-1">{{$clinica->nome}}</h3>
                     <div>{{$clinica->endereco}}</div>
                     <div>{{$clinica->cnpj}}</div>
                     <div>Telefone: {{$clinica->telefone}}</div>
                 </div>
                  <div class="col-sm-6">
                    
                     <h3 class="text-dark mb-1">Dados do Paciente</h3>
                     <div>Nome: {{$cliente->nome}}</div>
                     <div>CPF: {{$cliente->cpf}}</div>
                 </div>
             </div>
             <div class="imagens"><img src="img/dentesprontuario.fw.png" style="width: 100%;"></div>
             <div class="table-responsive-sm">
                 <table class="table table-striped" >
                     <thead>
                         <tr>
                             <th class="center">#</th>
                             <th>Região</th>
                             <th>Especialidade</th>
                             <th>Procedimento</th>
                         </tr>
                     </thead>
                     <tbody>
                     <input type="hidden" value="{{$count = 1 }}">
                    @foreach($dente_orcamento as $dentes)
                    <input type="hidden" value="{{$procedimento_name = App\Procedimento::where('id',$dentes->procedimento_id)->value('nome')}}">
                    <input type="hidden" value="{{$dente = App\Dente::where('id',$dentes->dente_id)->value('valor')}}">
                    <input type="hidden" value="{{$procedimento = App\Procedimento::where('id',$dentes->procedimento_id)->value('especialidade_id')}}">
                    <input type="hidden" value="{{$especialidade = App\Especialidade::where('id',$procedimento)->value('nome')}}">
                         <tr>
                             <td class="center">{{$count++}}</td>
                             <td class="left strong">{{ $dente }}</td>
                             <td class="left">{{$especialidade}}</td>
                             <td class="left">{{ $procedimento_name }}</td>
                         </tr>

                    @endforeach
                     </tbody>
                 </table>
             </div>
             <div class="row">
                 <div class="col-lg-4 col-sm-5">
                 </div>
                 <div class="col-lg-4 col-sm-5 ml-auto">
                     <table class="table table-clear">
                         <tbody>
                             <tr>
                                 <td class="left">
                                     <strong class="text-dark">Total</strong> </td>
                                 <td class="right">
                                    <input type="hidden" value="{{$preco = App\Orcamento::find($orcamento)}}">
                                     <strong class="text-dark">R${{$preco->preco}}</strong>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
         <div class="row assinatura">
             <div class="col-md-6 text-center">
                 ________________________________________<br>Assinatura do Paciente
             </div>
             <div class="col-md-6 text-center">
                 ________________________________________<br>Assinatura do Dentista
             </div>
         </div>
         <div class="card-footer bg-white">
             <p class="mb-0 float-left">Connecteeth</p>
             <p class="mb-0 float-right">23 de Julho de 2020</p>
         </div>
     </div>
 </div>


 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>