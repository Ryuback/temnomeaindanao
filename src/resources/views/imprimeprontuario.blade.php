 <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Prontuário</title>

    <style type="text/css">
        body {
    background-color: #000
}

.padding {
    padding: 2rem !important
}

.card {
    margin-bottom: 30px;
    border: none;
    -webkit-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    -moz-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22)
}

.card-header {
    background-color: #fff;
    border-bottom: 1px solid #e6e6f2
}

h3 {
    font-size: 20px
}

h5 {
    font-size: 15px;
    line-height: 26px;
    color: #3d405c;
    margin: 0px 0px 15px 0px;
    font-family: 'Circular Std Medium'
}

.text-dark {
    color: #3d405c !important
}

.borda {
    border-top: 1px solid #e6e6f2;
}

.assinatura{
    margin-bottom: 80px;
    margin-top: 50px;
}

.historico{
    margin-top: 35px;
}
    </style>
  </head>
  <body>



 <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
     <div class="card">
         <div class="card-header p-4">

           <h3 class="mb-0 float-left" >Dr.
            {{$users->name}}</h3> 

             <div class="float-right">
                 <h3 class="mb-0">CRODF {{$users->cro}}</h3>
                 
             </div>
         </div>
         <div class="card-body">
             <div class="row mb-4">
                 <div class="col-sm-12">
                    
                    <h3 class="text-dark mb-1">{{$clinica->nome}}</h3>
                    <div>{{$clinica->endereco}}</div>
                    <div>{{$clinica->cnpj}}</div>
                    <div>Telefone: {{$clinica->telefone}}</div>
                </div>
             </div>
              <div class="row mb-4 borda">
                  <div class="col-sm-12 mt-3">
                    
                     <h3 class="text-dark mb-1">Dados do Paciente</h3>
                     <div>Nome: {{$paciente->nome}}</div>
                     <div>CPF:  {{$paciente->cpf}}</div>
                     <div>Data de Nascimento:  {{date('d/m/Y', strtotime($paciente->datanascimento))}}</div>
                     <div>Telefone:  {{$paciente->telefone}}</div>
                     <div>CEP:  {{$paciente->cep}}</div>
                     <div>Endereço:  {{$paciente->endereco}}</div>
                     <div>Email:  {{$paciente->email}}</div>
                     <div>Sexo:  {{$paciente->sexo}}</div>
                 </div>
             </div>

            <input type="hidden" value="{{$resp1 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(0)->get()}}">
            <input type="hidden" value="{{$resp2 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(1)->get()}}">
            <input type="hidden" value="{{$resp3 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(2)->get()}}">
            <input type="hidden" value="{{$resp4 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(3)->get()}}">
            <input type="hidden" value="{{$resp5 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(4)->get()}}">
            <input type="hidden" value="{{$resp6 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(5)->get()}}">
            <input type="hidden" value="{{$resp7 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(6)->get()}}">
            <input type="hidden" value="{{$resp8 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(7)->get()}}">
            <input type="hidden" value="{{$resp9 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(8)->get()}}">
            <input type="hidden" value="{{$resp10 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(9)->get()}}">
            <input type="hidden" value="{{$resp11 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(10)->get()}}">
            <input type="hidden" value="{{$resp12 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(11)->get()}}">
            <input type="hidden" value="{{$resp13 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(12)->get()}}">
            <input type="hidden" value="{{$resp14 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(13)->get()}}">
            <input type="hidden" value="{{$resp15 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(14)->get()}}">
            <input type="hidden" value="{{$resp16 = App\Anamnese::where('prontuario_id', $busca_prontuario->id)->limit(1)->offset(15)->get()}}">
            

             <div class="table-responsive-sm">
                <h3 class="text-dark mb-1">Anamnese</h3>
                 <table class="table table-striped">
                     <tbody>
                         <tr>
                             <td class="left">Está em tratamento médico?</td>
                             <td class="right">
                                 @foreach ($resp1 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach
                            </td>  
                         </tr>
                         <tr>
                             <td class="left">Está tomando algum medicamento?</td>
                             <td class="right">
                                 @foreach ($resp2 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach</td>
                         </tr>
                         <tr>
                            <td class="left">Já teve alguma doença como hepatite, chagas, sifilis, febre reumática, câncer, HIV, etc?</td>
                            <td class="right">
                                @foreach ($resp3 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">É diabético?</td>
                            <td class="right"> 
                                @foreach ($resp4 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach</td>  
                        </tr>
                         <tr>
                            <td class="left">Sofre de alguma doença do coração?</td>
                            <td class="right">
                                 @foreach ($resp5 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach</td>  
                        </tr>
                         <tr>
                            <td class="left">É hipertenso?</td>
                            <td class="right">
                                @foreach ($resp6 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">É hemofilico?</td>
                            <td class="right">
                                @foreach ($resp7 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Seus pés incham com facilidade?</td>
                            <td class="right">
                                @foreach ($resp8 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Tem tosse persistente?</td>
                            <td class="right">
                                @foreach ($resp9 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Tem algum tipo de alergia?</td>
                            <td class="right">                               
                                @foreach ($resp10 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach</td>  
                        </tr>
                         <tr>
                            <td class="left">Quando se fere, demora para cicatrizar?</td>
                            <td class="right">
                                @foreach ($resp11 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Já foi submetido a anestesia para tratamento odontológico?</td>
                            <td class="right">
                                @foreach ($resp12 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Já teve hemorragia?</td>
                            <td class="right">
                                @foreach ($resp13 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Tem algum vício?</td>
                            <td class="right">
                                @foreach ($resp14 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach
                            </td>  
                        </tr>
                         <tr>
                            <td class="left">Está grávida?</td>
                            <td class="right">
                                @foreach ($resp15 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>

                         <tr>
                            <td class="left">Sofre de epilepsia?</td>
                            <td class="right">
                                @foreach ($resp16 as $resp)
                                @if ($resp->resposta == 1)
                                    Sim
                                @else
                                    Não
                                @endif
                             @endforeach    
                            </td>  
                        </tr>

                          <tr>
                             <td class="left">Observações:</td>
                             <td class="right">{{$busca_prontuario->obs}}</td>
                         </tr>

                     </tbody>
                 </table>
             </div>
             <div class="table-responsive-sm historico">
                <h3 class="text-dark mb-1">Histórico do paciente</h3>
             @foreach ($busca_orcamento as $busca_orcamentos) 
             <input type="hidden" value="{{$cliente = App\Orcamento::find($busca_orcamentos->id)}}">
             <input type="hidden" value="{{$dente_orcamento = DB::table('orcamento_has_dente')->where('orcamento_id', $cliente->id)->get()}}">
             <table class="table table-striped">
                <thead>
                    <tr>
                       <th>Região</th>
                        <th>Especialidade</th>
                        <th>Procedimento</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($dente_orcamento as $dentes)
                        <input type="hidden" value="{{$procedimento_name = App\Procedimento::where('id',$dentes->procedimento_id)->value('nome')}}">
                        <input type="hidden" value="{{$dente = App\Dente::where('id',$dentes->dente_id)->value('valor')}}">
                        <input type="hidden" value="{{$procedimento = App\Procedimento::where('id',$dentes->procedimento_id)->value('especialidade_id')}}">
                        <input type="hidden" value="{{$especialidade = App\Especialidade::where('id',$procedimento)->value('nome')}}">
                    
                             <tr>
                                 <td class="left strong">{{ $dente }}</td>
                                 <td class="left">{{$especialidade}}</td>
                                 <td class="left">{{ $procedimento_name }}</td>
                             </tr>

                        @endforeach
                    </tbody>
                </table>
                
         @endforeach

             </div>
         </div>
         <div class="card-footer bg-white">
             <p class="mb-0 float-left">Connecteeth</p>
             <p class="mb-0 float-right">23 de Julho de 2020</p>
         </div>
     </div>
 </div>


 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>