@extends('template')

@section('title', 'Criar orçamento')
@section('content')
<div class="container-fluid">

    <!-- Linha -->
    <div class="row">

        <div class="col-lg-12">

            <!-- Odontograma -->
            {!! Form::open(['route' => 'novoorcamento.store', 'method' => 'POST']) !!}
            <div class="card mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Odontograma</h6>
                </div>
                <div class="card-body" style="text-align: center;">
                    <table style="margin-left: auto; margin-right: auto;">
                        <tbody>

                            <tr>

                                @foreach ($dente1 as $dentes1)
                                <td class="colunadente">
                                    {{$dentes1->valor2}}
                                    <br> {{$dentes1->valor}}
                                    <br>
                                    <input class="{{$dentes1->nome}}" id="{{$dentes1->nome}}" type="checkbox" name="dente_id[]" onclick="denteSelecionado(this);" value="{{$dentes1->id}}"/>
                                    <label for="{{$dentes1->nome}}"></label>
                                </td>
                                @endforeach

                            <tr>
                                @foreach ($dente2 as $dentes2)
                                <td class="colunadente">
                                    <input class="{{$dentes2->nome}}" id="{{$dentes2->nome}}" type="checkbox" name="dente_id[]" onclick="denteSelecionado(this);" value="{{$dentes2->id}}"/>
                                    <label for="{{$dentes2->nome}}"></label>
                                    <br> {{$dentes2->valor}}
                                    <br> {{$dentes2->valor2}}
                                </td>
                                @endforeach
                            </tr>

                        </tbody>
                    </table>

                </div>
            </div>

        </div>

    </div>

    <!-- Linha -->
    <div class="row">

        <div class="col-lg-3">

            <!-- Odontograma -->
            <div class="card mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Especialidade</h6>
                </div>
                <div class="card-body">
                    <div class="list-group" id="myList" role="tablist">
                        <input type="hidden" value="{{$count = 0}}">
                        @foreach ($especialidade as $especialidades)
                        <input type="hidden" value="{{$count++}}">
                        <a class="list-group-item list-group-item-action @if ($count == 1) active @endif" data-toggle="list" href="#{{$especialidades->nome}}" role="tab">{{$especialidades->nome}}</a>
                        @endforeach
                  </div>
              </div>
          </div>

        </div>

        <div class="col-lg-3">

            <!-- Odontograma -->
            <div class="card mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Procedimento</h6>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <input type="hidden" value="{{$count = 0}}">
                    @foreach ($especialidade as $especialidades)
                        <input type="hidden" value="{{$count++}}">
                    <div class="tab-pane @if ($count == 1) active @endif" id="{{$especialidades->nome}}" role="tabpanel">
                        <div class="list-group" id="myProcedimentoList" role="tablist">
                            <input type="hidden" name="" value="{{$buscaprocedimento = App\procedimento::where('especialidade_id',$especialidades->id)->get()}}">
                            @foreach ($buscaprocedimento as $procedimentos)


                            <div class="custom-control custom-radio">
                              <input type="radio" id="{{$procedimentos->id}}" name="procedimento_id" class="custom-control-input" href="#{{$procedimentos->nome_id}}" value="{{$procedimentos->id}}">
                              <label class="custom-control-label" for="{{$procedimentos->id}}">{{$procedimentos->nome}}</label>
                            </div>

                            @endforeach

                        </div>

                </div>
                @endforeach
                    </div>
              </div>
          </div>

        </div>

        <div class="col-lg-6">

            <!-- Odontograma -->
            <div class="card mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Orçamento</h6>
                </div>
                <div class="card-body">
                      <div class="form-group">
                        <input type="hidden" value="{{$count = 0}}">
                        <div class="tab-content">
                                @foreach ($procedimento as $procedimentos)
                                <input type="hidden" value="{{$count++}}">
                                <div class="tab-pane @if ($count == 1) active @endif" id="{{$procedimentos->nome_id}}" name="{{$procedimentos->nome_id}}" role="tabpanel "><label>{{$procedimentos->nome}}</label></div> 
                                @endforeach
                        </div>

                        <input type="text" class="form-control" id="valor" name="valor" aria-describedby="region" placeholder="Valor do procedimento">
                        <small id="region" class="form-text text-muted"></small>
                    </div>
                    <button type="submit" class="btn btn-primary">Adicionar procedimento</button>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align:center">
                    <!-- <table style="text-align: center;"> -->
                    <tr>
                    <th>Procedimento</th>
                    <th>Dentes</th>
                    </tr>
                    @foreach($dente_orcamento as $dentes_orcamento)
                    <input type="hidden" value="{{$procedimento_name = App\Procedimento::where('id',$dentes_orcamento->procedimento_id)->value('nome')}}">
                    <input type="hidden" value="{{$dente = App\Dente::where('id',$dentes_orcamento->dente_id)->value('valor')}}">
                        <tr>
                        <td>{{$procedimento_name}}</td>
                        <td>{{$dente}}</td>
                        </tr>
                    @endforeach
                    </table>
                    <input type="hidden" value="{{$preco = App\Orcamento::find($orcamento)}}">
                    <br><br><hr class="sidebar-divider my-0"><br> Total  R$ {{$preco->preco}}

                    {!! Form::close()!!}

            </div>
        </div>

        </div>

    </div>

    <div class="row">

        <div class="col-lg-12">

            <!-- Odontograma -->
            <div class="card mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Paciente</h6>
                </div>
                <div class="card-body">
                    <form style="display: inline"  method="POST" action="{{ URL::to('/incluiorcamento') }}">
                        @csrf
                      <div class="form-group">
                            <select class="selectpicker" name="paciente" data-show-subtext="true" data-live-search="true">
                                <option disabled selected>Buscar Prontuário</option>
                                @foreach ($paciente as $pacientes)
                                <option data-subtext="{{$pacientes->cpf}}" value="{{$pacientes->id}}">{{$pacientes->nome}}</option>
                                @endforeach 
                            </select>
                        <br>
                        <small id="emailHelp" class="form-text text-muted">Selecione um paciente existente e clique em "Incluir a prontuário", ou clique em "Novo prontuário" para realizar um novo cadastro.</small>
                      </div>
                      
                      <a href="{{URL::to('/buscarorcamento')}}"><span type="#"class="btn btn-primary" >Salvar</span></a>
                      <button type="submit" class="btn btn-info">Incluir a prontuário</button>
                    </form>
                      <a href="/novoprontuario" class="btn btn-warning">Novo prontuário</a>
                      <span class="btn btn-secondary btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-print"></i>
                        </span>
                        <form style="display: inline" method="POST" action="{{ URL::to('/export') }}">
                            @csrf
                            <input type="hidden" value="{{$orcamento}}" name="id">
                            <button class="btn btn-text-white-50" style="color: white">Imprimir</button>
                            </form>
                        
                        </span>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- 
<script type="text/javascript">

    var dentesSelecionados = [];

    function denteSelecionado(checkboxDente) {
        if (dentesSelecionados.includes(checkboxDente.id)){
            dentesSelecionados.splice(dentesSelecionados.indexOf(checkboxDente.id), 1);
        } else {
            dentesSelecionados.push(checkboxDente.id);
        }


        var region = 'Região: ' + dentesSelecionados.join();
        var smallregion = document.getElementById('region');
        smallregion.innerHTML = region;
    }
</script> -->

@endsection