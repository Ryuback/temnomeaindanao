@extends('template')

@section('title', 'Novo prontuário')
@section('content')

<div class="container-fluid">
	<!-- Page Heading -->
	<div class="row accordion-gradient-bcg d-flex justify-content-center">
		<div class="col-md-10 col-xl-6 py-5">
			<!--Accordion wrapper-->
			<div class="accordion md-accordion accordion-2" id="accordionEx7" role="tablist" aria-multiselectable="true">
				<!-- Accordion card -->
				<div class="card">
					<div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading1">
						<a class="linkaccordion" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
							<h5 class="mb-0">Dados do Paciente</h5>
						</a>
					</div>
					<div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx7">
						<div class="card-body mb-1 rgba-grey-light white-text">
							<form method="POST" action="{{ URL::to('/savedata') }}">
								@csrf
								<div class="form-group">
									<label for="name">Nome*</label>
									<input type="text" class="form-control" name="nome" id="name" placeholder="Nome do Paciente" required value="{{$orcamento->nome}}" disabled>
								</div>
								<div class="form-group">
									<label for="cpf">CPF*</label>
									<input type="text" class="form-control" name="cpf" id="cpf" placeholder="Digite o CPF" required value="{{$orcamento->cpf}}" disabled>
								</div>
								<div class="form-group">
									<label for="data">Data de Nascimento*</label>
									<input type="date" class="form-control" name="data" id="data" placeholder="Selecione uma data" required>
								</div>
								<div class="form-group">
									<label for="name">Telefone*</label>
									<input type="text" class="form-control" name="telefone" data-mask="(00) 00000-0000" id="telefone" placeholder="Digite um número de telefone" required>
								</div>
								<div class="form-group">
									<label for="cep">CEP</label>
									<input type="text" class="form-control" name="cep" data-mask="00000-000" id="cep" placeholder="Digite um CEP">
								</div>
								<div class="form-group">
									<label for="endereco">Endereço</label>
									<input type="text" class="form-control" name="endereco" id="endereco" placeholder="Digite um Endereço">
								</div>
								<div class="form-group">
									<label for="email">E-mail</label>
									<input type="text" class="form-control" name="email" id="email" placeholder="Digite um Endereço de E-mail">
								</div>
								<fieldset class="form-group">
									<div class="form-group">
										<label for="email">Sexo</label>
										<div class="row radiogenero">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="sexo" id="sexo" value="masculino">
												<label class="form-check-label" for="inlineRadio1">Masculino</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="sexo" id="sexo" value="feminino">
												<label class="form-check-label" for="inlineRadio1">Feminino</label>
											</div>
										</div>
									</div>
								</fieldset>
						</div>
					</div>
				</div>
				<div class="card">
					<!-- Card header -->
					<div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading2">
						<a class="linkaccordion" class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
							<h5 class="mb-0">Anamnese</h5>
						</a>
					</div>
					<div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2" data-parent="#accordionEx7">
						<div class="card-body mb-1 rgba-grey-light white-text">
							
							<div class="row mb-3">
								<div class="col-8">Está em tratamento médico?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta1" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta1" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Está tomando algum medicamento?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta2" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta2" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Já teve alguma doença como hepatite, chagas, sifilis, febre reumática, câncer, HIV, etc?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta3" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta3" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">É diabético?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta4" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta4" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Sofre de alguma doença do coração?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta5" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta5" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">É hipertenso?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta6" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta6" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">É hemofilico?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta7" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta7" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Seus pés incham com facilidade?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta8" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta8" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Tem tosse persistente?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta9" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta9" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Tem algum tipo de alergia?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta10" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta10" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Quando se fere, demora para cicatrizar?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta11" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta11" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Já foi submetido a anestesia para tratamento odontológico?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta12" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta12" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Já teve hemorragia?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta13" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta13" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Tem algum vício?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta14" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta14" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Está grávida?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta15" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta15" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="row mb-3">
								<div class="col-8">Sofre de epilepsia?</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta16" id="gridRadios2" value="1"> Sim</div>
								<div class="col-2"><input class="form-check-input" type="radio" name="pergunta16" id="gridRadios2" value="0"> Não</div>
							</div>

							<hr class="sidebar-divider d-none d-md-block">

							<div class="form-group">
								<label for="exampleFormControlTextarea1">Observações:</label>
								<textarea class="form-control" name="obs" id="exampleFormControlTextarea1" rows="3"></textarea>
							</div>
						</div>
					</div>
				</div>

				<div style="float: right; bottom: 0; position: relative; margin-bottom: 15px">
				<button type="submit" class="btn btn-primary" value="Input">Salvar</button>
				</form>
			</div>
			</div>
		</div>
	</div>
</div>

@endsection