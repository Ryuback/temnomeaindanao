@extends('template') 

@section('title', 'Pacientes')
@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary corbusca">Dados pacientes</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Telefone</th>
                      <th>Prontuário</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Telefone</th>
                      <th>Prontuário</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($paciente as $pacientes)
                    <tr>
                      <td>{{$pacientes->nome}}</td>
                      <td>{{$pacientes->cpf}}</td>
                      <td>{{$pacientes->telefone}}</td>
                      <td>
                        <a href="#" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                            <i class="fas fa-file-pdf"></i>
                          </span>
                          <form method="POST" action="{{ URL::to('/exportprontuario') }}">
                            @csrf
                            <input type="hidden" value="{{$pacientes->id}}" name="id">
                            <button type="submit" class="text">Abrir</button>
                            </form>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $paciente->links() }}
              </div>
            </div>
          </div>

        </div>
@endsection