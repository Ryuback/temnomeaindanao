@extends ('template')
@section('title', 'Selecionar Consultório')
@section('content')

<div class="container-fluid">

    <!-- Content Row -->
    <div class="row d-flex justify-content-center">

        <div class="col-md-10 col-xl-6">

            <div class="card mb-12">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary corprontuario">Perfil do dentista</h6>
                </div>
                <div class="card-body">

                    <form  style="width: 100%;border:none;outline:none ;background-color:#fff;" method="POST" action="/updateperfil">
                    @csrf
                        <div class="form-group">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="nome" value="{{$user->name}}" disabled>
                        </div>
                         <div class="form-group">
                            <label for="email">Email</label>
                            <input type="mail" class="form-control" name="email" value="{{$user->email}}" disabled>
                        </div>
                         <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" class="form-control" name="telefone" data-mask="(00) 00000-0000" value="{{$user->telefone}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="cro">CRO</label>
                            <input type="number" class="form-control" name="cro" value="{{$user->cro}}" disabled>
                        </div>

                        <fieldset>
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="password" class="form-control" name="senha" placeholder="Digite a senha" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="confirmarsenha">Confirmar senha</label>
                                <input type="password" class="form-control" name="confirmarsenha" placeholder="Confirme a senha" value="" required>
                            </div>
                        </fieldset>


                        <div style="float: right; bottom: 0; position: relative; margin-bottom: 15px">
                            <button type="submit" class="btn btn-primary" value="Input" >Salvar alterações</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection