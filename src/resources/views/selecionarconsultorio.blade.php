@extends('template')

@section('title', 'Selecionar Consultório')
@section('content')


<input type="hidden" value="{{$clinica = $user_id -> buscarClinica}}">

@if($clinica_id == null)
@if ($message = Session::get('success'))
<div class="alert alert-danger" role="alert" style=" width:300px; position:relative; margin: 0 auto; text-align:center "> 
  <p>{{ $message }}</p>
</div>
@else
<div class="alert alert-primary" role="alert" style=" width:300px; position:relative; margin: 0 auto; text-align:center "> 
Nenhum Consultório Selecionado
</div>
@endif
                @else
                <div class="alert alert-success" role="alert" style=" width:300px; position:relative; margin: 0 auto; text-align:center ">
                Consultório Selecionado : {{$clinica_id->nome}}
                </div>
@endif


@foreach ($clinica as $clinicas)
<div class="container-type-small">

  <div class="teste-close">
  <form  method ="POST" action="{{route('selecionarconsultorio.destroy', $clinicas->id)}}">
  @csrf
  <input name="_method" type="hidden" value="DELETE">
  <button class="close" type="sumbit" onclick="return confirm('AVISO!!!\nAo excluir esse consultório, você não terá mais acesso ao mesmo.\nTem certeza que deseja prosseguir com a exclusão?')">
      <span aria-hidden="true">×</span>
    </button>
    </form>
  </div>
  
  <div class="logo-clinic">
    <img src="img/clinic.png" height="100%" width="100%">
  </div>
  
  <div class="name-clinic">
    <span style="color: black">{{$clinicas->nome}}</span><br>
    <span style="color: black">{{$clinicas->telefone}}</span>
  </div>
  <form  style="width: 100%; border:none; outline:none; background-color:#fff;" method="POST" action="{{ URL::to('/teste1') }}">
    @csrf
    <input type="hidden" value="{{$clinicas->id}}" name="id">
    <button type="submit" style="height:100%; width: 100%; border:none; outline:none; background-color:#fff;"></button>
  </form>
</div>
@endforeach


<a href="#" data-toggle="modal" data-target="#consultorioModal">
    <div class="container-type-small" style="margin-bottom: 0px;">
    <div class="more-plus"><i class="fas fa-plus-square"></i></div>
  </div>

</a>



@endsection