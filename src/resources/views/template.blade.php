<?php 
    //Pega caminho da URL atual para definir menu selecionado
    $link = $_SERVER['REQUEST_URI'];
    $link_array = explode('/',$link);
    $page = end($link_array);
    $selectedmenu = $page;
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title') - ConnecTeeth</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.css" rel="stylesheet">
  <link href="css/estilo.css" rel="stylesheet">
  <link href="css/dentes.css" rel="stylesheet">
  <link rel="stylesheet" href="css/container.css">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css' rel='stylesheet' />
  <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css' rel='stylesheet' />
  
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.min.js'></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
        <div class="sidebar-brand-icon">
          <i class="fas fa-tooth"></i>
        </div>
        <div class="sidebar-brand-text mx-3">ConnecTeeth</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Menu inicio -->
      <li class="nav-item <?php if ($selectedmenu == 'home') { echo('active'); } ?>">
        <a class="nav-link" href="/home">
          <i class="fas fa-fw fa-home"></i>
          <span>Início</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Menu agenda-->
      <li class="nav-item <?php if ($selectedmenu == 'agenda') { echo('active'); } ?>">
        <a class="nav-link" href="/agenda">
          <i class="fas fa-fw fa-calendar-alt"></i>
          <span>Agenda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Menu pacientes -->
      <li class="nav-item <?php if ($selectedmenu == 'pacientes') { echo('active'); } ?>">
        <a class="nav-link" href="/pacientes">
          <i class="fas fa-fw fa-user"></i>
          <span>Pacientes</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Menu orçamentos -->
      <li class="nav-item <?php if ($selectedmenu == 'informardados' || $selectedmenu == 'buscarorcamento') { echo('active'); } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-money-check-alt"></i>
          <span>Orçamentos</span>
        </a>
        <div id="collapseTwo" class="collapse <?php if ($selectedmenu == 'informardados' || $selectedmenu == 'buscarorcamento') { echo('show'); } ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?php if ($selectedmenu == 'informardados') { echo('active'); } ?>" href="/informardados">Novo</a>
            <a class="collapse-item <?php if ($selectedmenu == 'buscarorcamento') { echo('active'); } ?>" href="/buscarorcamento">Buscar </a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Botão selecionar consultório -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="/selecionarconsultorio">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                  Selecionar Consultório
                </span>
              </a>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{$user_id = auth()->user()->name}}</span>
                <img class="img-profile rounded-circle" src="img/fotoperfil.fw.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="/perfil">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
          @yield('content')
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; ConnecTeeth 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deseja sair?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Selecione "Sair" para finalizar sua sessão.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
           {{ __('Sair') }}
       </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">  
            @csrf
            {{--  <button type="submit" class="btn btn-primary" data-dismiss="modal">Sair</button>  --}}
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="consultorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Novo consultório</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
       


<div class="card">
          
          <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx7">
            <div class="card-body mb-1 rgba-grey-light white-text">
              {!! Form::open(['route' => 'selecionarconsultorio.store', 'method' => 'POST']) !!}
                <div class="form-group">
                  <label for="name">Nome*</label>
                  <input type="text" class="form-control" name="nome" placeholder="Nome da clinica" required>
                </div>
                <div class="form-group">
                  <label for="cnpj">CNPJ*</label>
                  <input type="text" class="form-control" name="cnpj" data-mask="00.000.000/0000-00" placeholder="Digite o CNPJ" required>
                </div>
                <div class="form-group">
                  <label for="name">Telefone*</label>
                  <input type="text" class="form-control" name="telefone" data-mask="(00) 00000-0000" placeholder="Digite um número de telefone" required>
                </div>
                <div class="form-group">
                  <label for="cep">Endereço</label>
                  <input type="text" class="form-control" name="endereco" placeholder="Digite um Endereço" required>
                </div>
               

             <div style="float: right; bottom: 0; position: relative; margin-bottom: 15px">
                <button type="submit" class="btn btn-primary" value="Input" >Salvar</button>
             </div>
             {!! Form::close()!!}

            </div>
          </div>
        </div>



      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-pt_BR.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
</body>

</html>


