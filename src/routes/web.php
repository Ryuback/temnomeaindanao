<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/login', function () {
    return view('login');
});
Route::get('/login1', function () {
    return view('login1');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('confirmexclude/{id}', 'AgendaController@confirmexclude');

Route::resource('pacientes', 'PacientesController');

Route::resource('novoorcamento', 'OrcamentoController');

Route::resource('agenda', 'AgendaController');


Route::get('/buscarorcamento', 'OrcamentoController@buscaorcamento');

Route::get('novoprontuario', 'OrcamentoController@novoprontuario');

Route::get('/forgot-password',function(){
    return view('forgot-password');
});


Route::resource('/perfil', 'PerfilController');
Route::post('/updateperfil', 'PerfilController@updateperfil');

Route::post('/teste1', 'ConsultorioController@teste');

Route::post('/export', 'ExportController@index');

Route::post('/exportprontuario', 'ExportController@exportprontuario');

Route::post('/solicitadados', 'OrcamentoController@solicitadados');

Route::get('/informardados', 'OrcamentoController@informardados');

Route::post('/incluiorcamento', 'OrcamentoController@inclueorcamento');

Route::resource('selecionarconsultorio', 'ConsultorioController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/savedata', 'AnamneseController@requestdata');

Route::get('deleteagendamento/{idagendamento}', 'AgendaController@destroy');


