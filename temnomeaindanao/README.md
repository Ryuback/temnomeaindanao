# PROJETO TCC

Requerimentos

1. Vagrant;
2. VirtualBox;
3. GitBash;
4. Acesso a Internet =D;

# Como instalar o Laravel

Obs:. A partir daqui é tudo com o GitBash

1. Entre na pasta que desejar ( "cd $usr/Desktop/pastaquevcquer" ) e clone o repositório usando o comando -> "git clone https://gitlab.com/Ryuback/temnomeaindanao.git";

2. Dentro da pasta clonada execute o "vagrant up" ele irá automaticamente baixar a ISO do server ubuntu e vai montar o servidor de desenvolvimento pra você, após isso é só esperar, qualquer erro inesperado me procure -> (061) 98156-5313;

3. Após iniciado você conseguirá acessar o projeto botando o seguinte IP no seu Navegador "192.168.4.18";

4. O código do seu projeto encontra-se na pasta /src, Divirta-se;
